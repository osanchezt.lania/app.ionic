export interface Creditos {
    "id"?: number;
    "nombre_ac": string;
    "clientes": number;
    "monto": number;
    "nombrebanco": string;
    "fecha_desembolso": string;
    "nombresucursal": string;
    "nombreregional": string;
}