export interface Desembolso {
    "id"?: number;
    "nombre_ac": string;
    "clientes": number;
    "monto": number;
    "banco": number;
    "fecha_desembolso": string;
    "sucursal": number;
    "regional": number;
}