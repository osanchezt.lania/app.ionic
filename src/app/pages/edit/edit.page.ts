import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Creditos } from '../../models/Creditos';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  seenButtonEdit: boolean = true;
  seenButtonUpdate: boolean = false;
  inputDisabled: boolean = true;
  desembolso: any = [];/*{
    nombre_ac: '',
    clientes: null,
    monto: null,
    idbanco: null,
    fecha_desembolso: '',
    idsucursal: null,
    idregional: null
  };*/
  credito: Creditos[] = [];

  constructor(
    private dataService: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController, 
    private toastController: ToastController,
  ) { }

  ngOnInit() {
    this.getCreditoById();
  }

  getCreditoById() {
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.dataService.getCreditoById(params.id).subscribe(
        (res: any) => {
          this.credito = res;
          console.log(this.credito);
        },
        err => console.log(err)
      );
    }
  }

  eidtCredit() {
    this.seenButtonEdit = false;
    this.seenButtonUpdate = true;
    this.inputDisabled = false;
  }

  async updateCredit(item){
    const alert = await this.alertController.create({
      header: '¿Datos correctos?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancelado...');
            }
          },
          {
            text: 'Actualizar',
            handler: () => {
              /*const monto_string = item.monto;
              const monto_separado = monto_string.split(".");
              const monto_formateado = monto_separado[0];
              const number_monto = Number(monto_formateado)
              item.monto = number_monto;*/
              this.desembolso = item;
              this.message('Datos actualizados');
              this.dataService.updateCredito(this.desembolso.id, this.desembolso).subscribe(
                res => {
                  console.log(res);
                  //console.log(this.desembolso);
                  this.router.navigate(['/financieros/home']);
                },
                err => console.log(err)
              );
            }
          }
        ]
    });
    await alert.present();
  }

  async message(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

}
