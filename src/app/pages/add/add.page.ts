import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { Desembolso } from '../../models/Desembolso';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  creditForm: FormGroup;
  desembolso: Desembolso /*= {
    nombre_ac: '',
    clientes: null,
    monto: null,
    idBanco: null,
    fecha_desembolso: '',
    idSucursal: null,
    idRegional: null
  }*/;

  constructor(
    private dataService: DataService, 
    private alertController: AlertController, 
    private toastController: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
    this.creditForm = this.createFormGroup();
  }

  async saveCredit() {
    if (this.creditForm.valid) {
      this.desembolso = this.creditForm.value;
      const alert = await this.alertController.create({
        header: '¿Datos correctos?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancelado...');
            }
          },
          {
            text: 'Guardar',
            handler: () => {
              this.message('Datos guardados satisfactoriamente');
              //console.log(this.desembolso);
              this.dataService.saveCredito(this.desembolso).subscribe(
                res => {
                  console.log(res);
                  this.creditForm.reset();
                  this.router.navigate(['/financieros/home'])
                },
                err => console.log(err),
              );
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.message('Completa todos los campos');
    }
  }

  resetForm() {
    this.creditForm.reset();
  }

  async message(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  createFormGroup() {
    return new FormGroup({
      nombre_ac: new FormControl('', Validators.required),
      clientes: new FormControl('', Validators.required),
      monto: new FormControl('', Validators.required),
      banco: new FormControl('', Validators.required),
      fecha_desembolso: new FormControl('', Validators.required),
      sucursal: new FormControl('', Validators.required),
      regional: new FormControl('', Validators.required),
    });
  }
  get nombre_ac() { return this.creditForm.get('nombre_ac'); }
  get clientes() { return this.creditForm.get('clientes'); }
  get monto() { return this.creditForm.get('monto'); }
  get banco() { return this.creditForm.get('banco'); }
  get fecha_desembolso() { return this.creditForm.get('fecha_desembolso'); }
  get sucursal() { return this.creditForm.get('sucursal'); }
  get regional() { return this.creditForm.get('regional'); }

}
