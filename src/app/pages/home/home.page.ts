import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { Creditos } from '../../models/Creditos';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  creditos: Creditos[] = [];
  subscribe: any;

  constructor(
    private dataService: DataService,
    private alertController: AlertController, 
    private toastController: ToastController,
    private loadingCtrl: LoadingController,
    //public platform: Platform,
    ) {
      /*this.subscribe = this.platform.backButton.subscribeWithPriority(666666, () => {
        if(this.constructor.name === "HomePage") {
          if(window.confirm("¿Cerrar app?")) {
            navigator["app"].exitApp();
          }
        }
      })*/
    }

  ngOnInit() {
  }

  /*ionViewDidEnter(){
    console.log('Cargado desde ionViewDidEnter');
  }*/

  ionViewWillLeave(){
    this.creditos = [];
  }

  ionViewWillEnter() {
    this.getData();
  }

  async getData() {
    const loading = await this.loadingCtrl.create({
      message: 'Cargando...',
    });
    await loading.present();
    this.dataService.getCreditos().subscribe(async (res: any) => {
      this.creditos = res;
      console.log(this.creditos);
      await loading.dismiss();
    })
  }

  async deleteCredit(id: number){
    const alert = await this.alertController.create({
      header: '¿Eliminar?',
      message: `¿Eliminar registro con ID: ${id}?`,
      buttons: [
        {
          text: `Cancelar`,
          role: 'cancel',
          handler: () => {
            console.log('Cancelado...');
          }
        },
        {
          text: 'Eliminar',
            handler: () => {
              this.dataService.deleteCredito(id).subscribe(
                res => {
                  console.log(res);
                  this.getData();
                },
                err => console.log(err),
              );
              this.message('Registro con ID ' + id + ' eliminado.')          
            }
        }
      ]
    });
    await alert.present();
  }



  async message(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

}
