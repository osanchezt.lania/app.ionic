import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Creditos } from '../models/Creditos';
import { Desembolso } from '../models/Desembolso';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //URI = 'http://localhost:4000/operaciones';
  URI = 'https://app-operaciones.herokuapp.com/operaciones';
  public desembolsos = new BehaviorSubject([]);

  constructor(private http: HttpClient) { }

  public getDesembolsos(): Observable<Desembolso[]> {
    return this.desembolsos.asObservable();
  }

  getCreditos() {
    return this.http.get(this.URI);
  }

  /*getCreditos(): Observable<Creditos> {
    return this.http.get<Creditos>(this.URI);
  }*/

  getCreditoById(id: number): Observable<Creditos> {
    return this.http.get<Creditos>(this.URI + `/${id}`);
  }

  saveCredito(desembolso: Desembolso): Observable<Desembolso> {
    return this.http.post<Desembolso>(this.URI, desembolso);
  }

  deleteCredito(id: number): Observable<Desembolso> {
    return this.http.delete<Desembolso>(this.URI + `/${id}`);
  }

  updateCredito(id: number, desembolso: Desembolso): Observable<Desembolso> {
    return this.http.put<Desembolso>(this.URI + `/${id}`, desembolso);
  }
}
