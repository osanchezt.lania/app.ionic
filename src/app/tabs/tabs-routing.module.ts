import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'financieros',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../pages/home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'add',
        loadChildren: () => import('../pages/add/add.module').then(m => m.AddPageModule)
      },
      {
        path: 'login',
        loadChildren: () => import('../pages/login/login.module').then(m => m.LoginPageModule)
      },
      {
        path: 'home/edit/:id',
        loadChildren: () => import('../pages/edit/edit.module').then(m => m.EditPageModule)
      },
      {
        path: '',
        redirectTo: '/financieros/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/financieros/home',
    pathMatch: 'full'
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
